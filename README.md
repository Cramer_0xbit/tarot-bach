# Definiones de la baraja

## Marco salmón.   Grupo 1: Miedo. Sendero de la Compasión
### 26-Rock Rose. (Helianthemun Vulgare)
Si en estos momentos el miedo puede más que la motivación, lánzate a probar tus capacidades… ¡te sorprenderás!

### 20-Mimulus. (Mimulus Luteus)  
Si te centras en aquello que temes nunca saldrás hacia nuevas experiencias. Explora tus miedos y sal afuera. Prueba y verás que todo es más sencillo de lo parece.

### 6-Cherry Plum: (Prunus Cerasífera)
La mente tranquila es fruto del descubrimiento progresivo de tus sombras. 

### 2-Aspen: (Populus Trémula)
Déjate llevar a través del bosque de tu camino interior. Abraza tus sentimientos y expresa. Expresa sin temor.

### 25-Red Chestnut: (Aesculus Carnea)
Pierde el miedo personal por los demás, cultiva la confianza plena y confía en el abrazo protector de la madre Naturaleza
>falta archivo

## Marco amarillo.   Grupo 2: Incertidumbre. 

### 5-Cerato. (Ceratostigma Willmottiana)
No olvides que la clave está en conectar con tu silencio interior. En tu interior hay alguien que sabe, que te guía. Escucha esas señales y síguelas antes de que el ruido de los demás te despiste.

### 28-Scleranthus. (Scleranthus Annuus)
 Independientemente de si tu decisión será o no la más correcta y productiva es importante decidirse. Esto es en sí un acierto que te irá conduciendo progresivamente a la fortaleza interior.

### 12-Gentian. (Gentiana Amarella)
Una duda a tiempo puede ayudarte a reflexionar. Dudar ha de convertirse en un camino seguro hacia la certidumbre.
>falta archivo

### 13-Gorse. (Ulex Europaeus)
Es importante que enfoques tu energía en recuperar la fuerza y las ganas de luchar. El poder de realización está en tu interior para ser expresado.

### 17-Hornbeam. (Carpinus Betulus)
La vida es un pulso entre el hacer y el no hacer. Para triunfar se ha de encontrar la medida para saber activarse a tiempo, para saber detenerse a tiempo igual que lo hace la naturaleza durante las estaciones.


### 36-Wild Oat. (Bromus Asper)
Antes de irte siente que has apurado el aprendizaje. Sé un equilibrista que sabe mantenerse en la cuerda floja hasta entender tu dirección.


## Marco azul claro.    Grupo 3: Falta de interés por el presente. Sendero del Altruismo.

### 9-Clematis. (Clematis Vitalba)
Estar dormido es cerrar los ojos a la realidad. ¡Despierta!  Abre tus ojos, conecta con tu cuerpo, mueve tus pies, que nada te pare para materializar tus sueños.

### 16-Honeysuckle. (Lonicera Caprifolium)
Lo que ya viviste está en ti. Deja atrás lo que se fue y dirígete hacia lo que viniste a ser. La llave que abre el poder del aquí y del ahora está en tu mano.

### 37-Wild Rose. (Rosa Canina)
Para el corazón, cada día es como un lienzo en blanco que ha de colorearse, sin importar lo que pasó ayer, sin creer saber lo que pasará mañana.
>falta archivo


### 23-Olive. (Olea Europaea)
Descansa para poder seguir adelante. Reponer fuerzas es el preámbulo para volver a la acción.
>falta archivo
### 35-White Chestnut. (Aesculus Hippocastanum)
Para permitir que tu mente se relaje concéntrate en lo que estás haciendo, y convéncete de que lo que sea… será

### 21-Mustard. (Sinapsis Arvensis)
El hilo de la vida se encuentra y se pierde, siempre está en movimiento pero nunca es apresable.
>falta archivo

### 7-Chestnut Bud. (Aesculus Hippocastanum)
Hay una escuela llamada vida. Estate atento y en ella encontrarás todo lo que necesitas.
>falta archivo

## Marco Violeta. Grupo 4: Soledad. Sendero de la Humildad

### 18-Impatients. (Impatients Royalei)
De Paz y Ciencia es el agua con que riego mis semillas y así crecen a su ritmo, en su momento, a su tiempo.

### 34-Water Violet. (Hottonia Palustris)
Tus más altas capacidades multiplican su beneficio cuando las orientas a un bien común.

### 14-Heather. (Calluna Vulgaris)
La sabiduría se manifiesta en ti con el buen empleo de tus palabra. Observa cuanto dices y cómo lo dices.

## Marco verde.   Grupo 5: Hipersensibilidad a influencias y opiniones. Sendero del Amor

### 1-Agrimony. (Agrimonia Eupatoria)
Si actúas como no eres, lo que eres tarde o temprano te atormentará. Libera a tu alma de la jaula de las apariencias

### 4-Centaury. (Erythraea Centarium)
Ningún peregrino debe de olvidar su propio camino. Recuerda qué has venido a hacer, para que todo lo que das, te fortalezca

### 33-Walnut. (Juglans Regia)
Se firme en tus opiniones y criterios. Ve adelante con ellos siguiendo el sendero del corazón. Cruza el puente y llega imperturbable a la otra orilla

### 15-Holly. (Ilex Aquifolium)
La mayor victoria se asienta en la guerra que has sabido evitar.

## Marco Rosa.   Grupo 6: Abatimiento. Sendero de la sabiduría.

### 19-Larch. (Larix Europea)
La confianza en uno mismo se conquista peldaño a peldaño.  Aprende a diferenciar las limitaciones verdaderas de las justificaciones.

### 24-Pine. (Pinus Sylvestris)
No todos los sentimientos de deuda son reales. No cargues con lo que no es tuyo. El Perdón te hace libre

### 11-Elm. (Ulmus Campestris)
Fluye como un rio de comprensión en la grandiosidad, permitiendo que los elementos se ordenen sin importarte cuando habrá que pararse a descansar

### 30-Sweet Chestnut. (Castanea Vulgaris)
La realidad llega a veces como un fuerte rayo que angustia para después descubrir que todo final guarda la semilla de un nuevo principio

### 29-Star of Bethlehem. (Ornithogalum Umbellatum) 
Emerjo desde el dolor, como una reina que ha cruzado un océano de fuego y ha vencido.

### 38-Willow. (Salix Vitellina)
Bajo la Luz de la Luna recordé… ¡Cuantas veces he sentido y siento el dulce néctar del Amor! Y… ¿Por qué lo olvido?
>falta archivo

### 22-Oak. (Quercus Pedunculata)
¿Has probado a despertar en ti la imaginación? Recuerda que no todo se resuelve mediante el esfuerzo.

### 10-Crab Apple. (Pyrus Malus)
Siente en tu interior la energía limpia, pura y cristalina de un baño en plena naturaleza. 

## Marco azul. Grupo 7: Sobreprotección y excesiva preocupación por el bienestar de los demás. Sendero de la libertad.

### 8-Chicory. (Cichorium Intybus)
El amor es confianza plena, apoyo, respeto y libertad, el amor es capaz de edificarse por encima de las preocupaciones. Llegar a esto implica un gran trabajo con uno mismo.

### 31-Vervain. (Verbena Officinalis)
Lo que da forma a tu vida no es la obstinación si no la capacidad de comprender lo que se puede cambiar y lo que no

### 32-Vine. (Vitis Vinifera)
Piensa en tu autoridad interior. Todo tiene un límite, para el que manda y para el que obedece. No todo vale. El respeto es la base del amor interpersonal.
### 3-Beech. (Fagus Sylvatica)
Observa cómo la crítica se convierte en un desierto interno. Mira hacia adentro. Transforma progresivamente la palabra detractora en auto-observación.

### 27-Rock Water. Agua de Roca.
Si despiertas tu capacidad de adaptación a lo diferente, si te liberas de lo establecido, el progreso que esperas sucede.

### 0-Vitalis Rotam. La Rueda de la Vida.
En una transformación interminable gira y gira la rueda de la vida, incesante, rítmica ¡Perfecta!
